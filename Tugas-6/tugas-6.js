//soal no.1
const LuasLingkaran = (r) =>{
    return Math.PI * r * r;
}

const KelilingLingkaran = (r) => {
    return 2 * Math.PI * r
}

console.log("Luas Lingkaran: " + LuasLingkaran(6)) + "cm";
console.log("Keliling Lingkaran: " + KelilingLingkaran(8) + "cm");


//soal no.2
let kalimat = ""

console.log(kalimat = `saya`);
console.log(kalimat += `\nadalah`);
console.log(kalimat += `\nseorang`);
console.log(kalimat += `\nfrontend`);
console.log(kalimat += `\ndeveloper`);


//soal no.3
const newFunction = function literal(firstName, lastName){
    return {
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
newFunction("William", "Imoh").fullName();

//soal no.4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const {firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName, lastName, destination, occupation);

//soal no.5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)
