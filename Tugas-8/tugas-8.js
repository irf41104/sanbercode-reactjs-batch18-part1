// soal 1
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
let time = 10000;
 
readBooks(time, books[0], function(item1){
    readBooks(item1, books[1], function(item2){
        readBooks(item2, books[2], function(item3){
            readBooks(item3, books[3], function(item4){
                console.log(item4);
            })
        })
    })
})




// soal 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
let time = 10000;
 
readBooksPromise(time, books[0]).then(
   function(item1){ 
    readBooksPromise(item1, books[1]).then( 
        function(item2){
            readBooksPromise(item2, books[2]).then(
                function(item3){   
                    console.log(item3);
                })
        })
    })
